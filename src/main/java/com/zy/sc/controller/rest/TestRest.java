package com.zy.sc.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Geeyo on 2019/1/2.
 */
@RequestMapping("api/test")
@RestController
public class TestRest {

    @Autowired
    private DiscoveryClient client;

    Logger logger = LoggerFactory.getLogger(TestRest.class);

    @RequestMapping("hello")
    public String hello() {
        ServiceInstance instance = client.getLocalServiceInstance();
        logger.info("/hello,host:" + instance.getHost() + ",service_id:" + instance.getServiceId());
        return "helloWorld";
    }
}
